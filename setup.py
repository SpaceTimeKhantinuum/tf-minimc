#!/usr/bin/env python

from distutils.core import setup
from setuptools import find_packages

# https://stackoverflow.com/questions/26900328/install-dependencies-from-setup-py
import os
thelibFolder = os.path.dirname(os.path.realpath(__file__))
requirementPath = thelibFolder + '/requirements.txt'
#install_requires = []  # Examples: ["gunicorn", "docutils>=0.3", "lxml==0.5a7"]
if os.path.isfile(requirementPath):
   with open(requirementPath) as f:
       install_requires = f.read().splitlines()

setup(name='tf_minimc',
      version='0.0.1',
      description='Simple MCMC with TensorFlow',
      author='Sebastian Khan',
      author_email='khans22@cardiff.ac.uk',
      packages=find_packages(),
      install_requires=install_requires,
      url='https://gitlab.com/SpaceTimeKhantinuum/tf-minimc'
      )

