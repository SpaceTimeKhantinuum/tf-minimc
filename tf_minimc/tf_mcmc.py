"""
This code is basically the small mcmc code from
https://colindcarroll.com/2019/08/18/very-parallel-mcmc-sampling/
but without the vectorisation to parallel chains for simplicity

and translated into tensorflow code

"""

import tensorflow as tf
import tensorflow_probability as tfp
tfd = tfp.distributions

def metropolis_hastings(log_prob, proposal_cov, iters, init, dtype):
    """Metropolis-Hastings using tensorflow

    log_prob: function to compute log probability of likelihood
    proposal_cov: covarianance matrix for proposal distribution
    iters: number of steps to run MCMC for
    init: array shape(1, N) where N is the dimensionality of the space
    dtype: defaults to tf.float32

    returns
        samples: array shape(iter, N) accepted samples from MCMC
    """
    proposal_cov = tf.cast(proposal_cov, dtype)
    init = tf.cast(init, dtype)
    dim = proposal_cov.shape[0]

    # Using TensorArray allows us to emulate numpy array
    samples = tf.TensorArray(dtype=dtype, size=iters, clear_after_read=False)
    
    samples = samples.write(0, init)
    current_log_prob = log_prob(init)

    loc = tf.zeros(dim, dtype)
    scale = tf.linalg.cholesky(proposal_cov)
    proposals_dist = tfd.MultivariateNormalTriL(loc=loc, scale_tril=scale)
    proposals = proposals_dist.sample(iters-1)

    log_unifs = tf.math.log(tf.random.uniform([iters-1], dtype=dtype))

    for idx in tf.range(1, iters-1):
        log_unif = log_unifs[idx]
        sample = proposals[idx]

        prev_sample = samples.read(idx-1)

        proposal = sample + prev_sample
        proposal_log_prob = log_prob(proposal)

        # accept is False (zero) or True (one)
        accept = (log_unif < proposal_log_prob - current_log_prob)

        if accept:
            proposal = tf.cast(proposal, dtype)
        else:
            proposal = prev_sample

        samples = samples.write(idx, proposal)
        current_log_prob = proposal_log_prob

    # use samples.stack() to return the Tensor from the TensorArray
    return samples.stack()

def tf_raw_metropolis_hastings(log_prob, proposal_cov, iters, init, dtype=tf.float32):
    return metropolis_hastings(log_prob, proposal_cov, iters, init, dtype)

@tf.function
def tf_graph_metropolis_hastings(log_prob, proposal_cov, iters, init, dtype=tf.float32):
    return metropolis_hastings(log_prob, proposal_cov, iters, init, dtype)

@tf.function(experimental_compile=True)
def tf_XLA_metropolis_hastings(log_prob, proposal_cov, iters, init, dtype=tf.float32):
    return metropolis_hastings(log_prob, proposal_cov, iters, init, dtype)
