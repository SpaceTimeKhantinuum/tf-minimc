"""
This code is basically the small mcmc code from
https://colindcarroll.com/2019/08/18/very-parallel-mcmc-sampling/
but without the vectorisation to parallel chains for simplicity
"""

import numpy as np

def metropolis_hastings(log_prob, proposal_cov, iters, init):
    """Metropolis-Hastings using numpy

    log_prob: function to compute log probability of likelihood
    proposal_cov: covarianance matrix for proposal distribution
    iters: number of steps to run MCMC for
    init: array shape(1, N) where N is the dimensionality of the space

    returns
        samples: array shape(iter, N) accepted samples from MCMC
    """
    dim = proposal_cov.shape[0]

    samples = np.empty((iters, dim))
    samples[0] = init
    current_log_prob = log_prob(init)

    proposals = np.random.multivariate_normal(
        np.zeros(dim), proposal_cov, size=iters-1)

    log_unifs = np.log(np.random.rand(iters-1))

    for idx, (sample, log_unif) in enumerate(zip(proposals, log_unifs), start=1):
        proposal = sample + samples[idx-1]
        proposal_log_prob = log_prob(proposal)
        # accept is False (zero) or True (one)
        accept = (log_unif < proposal_log_prob - current_log_prob)

        if accept:
            samples[idx] = proposal
        else:
            samples[idx] = samples[idx-1]

        current_log_prob = proposal_log_prob

    return samples

